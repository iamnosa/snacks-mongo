import json
from bson import json_util

def to_json(data):
    return json.dumps(data, default=json_util.default)
