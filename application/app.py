'''
Entry point to the application
'''
from flask import Flask, jsonify, request
from pymongo import MongoClient
from bson import json_util
from bson.objectid import ObjectId
import json

#initialize Flask
app = Flask(__name__)

#mongodb necessary configuration
client = MongoClient('localhost:27017')
db = client.Snacks


#the endpoint to get all types of Snacks
@app.route('/api/v2/snacks/types', methods=['GET'])
def show_all_types():
    #get snacks from db
    snacks = db.Snacks.find()
    #empty snack_list to hold our filtered snacks
    snack_list = []
    for s in snacks:
        snack_list.append({'id':str(s['_id']), 'name':s['name'], 'description':s['description'], 'rate':s['rate'], 'in-stock':s['in-stock']})
    return jsonify({'snacks':snack_list})


#get a type based on the ID passed
@app.route('/api/v2/snacks/types/<type_id>', methods=['GET'])
def get_type(type_id):
    #empty output string to hold our output
    output = []
    #get all snacks
    snacks = db.Snacks.find()
    #filter out the snacks based on the ID
    snack_list = [s for s in snacks if s['_id'] == ObjectId(type_id)]

    if len(snack_list) == 0:
        return jsonify({'error':'Please enter a valid ID'})
    else:
        for s in snack_list:
            output.append({'id':str(s['_id']), 'name':s['name'], 'description':s['description'], 'rate':s['rate'], 'in-stock':s['in-stock']})
        return jsonify({'snack':output})


#create a snack type through the values posted
@app.route('/api/v2/snacks/types', methods=['POST'])
def create_type():
    #empty list to hold our output
    output = []

    #get all the variables from the request.json
    name = request.json['name']
    rate = request.json['rate']
    description = request.json['description']
    in_stock = request.json['in-stock']

    #try inserting the values into the db
    try:
        #insert the new Snack and get the id
        db.Snacks.insert({'name':name, 'description':description, 'rate':rate, 'in-stock':in_stock})
    except:
        #return an error
        return jsonify({'error':'please insert correct data'})

    #record the output
    output.append({'name':name, 'description':description, 'rate':rate, 'in-stock':in_stock})

    #get the new snack
    return jsonify({'message':'success'},{'snack':output})


#update a type
@app.route('/api/v2/snacks/types/<type_id>', methods=['PUT'])
def overwrite_type(type_id):
    #empty list to hold the output
    output = []
    #empty list to hold the values of the previous record
    old = []

    #get the values from the request
    name = request.json['name']
    rate = request.json['rate']
    description = request.json['description']
    in_stock = request.json['in-stock']

    #get the old details
    snacks = db.Snacks.find()
    snack_list = [s for s in snacks if s['_id'] == ObjectId(type_id)]
    for s in snack_list:
        old.append({'id':str(s['_id']), 'name':s['name'], 'rate':s['rate'], 'description':s['description'], 'in-stock':s['in-stock']})

    #Try update the Document with the specified ID
    try:
        db.Snacks.update_one(
            {'_id':ObjectId(type_id)},
            {
            '$set':{
                    'name':name,
                    'rate':rate,
                    'description':description,
                    'in-stock':in_stock
            }
            }
        )
        output.append({'id':type_id, 'name':name, 'description':description, 'rate':rate, 'in-stock':in_stock})
        return jsonify({'message':'success'}, {'old-content':old}, {'new-content':output})
    except:
        return jsonify({'error':'record not inserted'})


#delete a record
@app.route('/api/v2/snacks/types/<type_id>', methods=['DELETE'])
def delete_type(type_id):
    #empty list that would hold the values for the old Document
    old = []
    #try get the previous version before delete
    try:
        snacks = db.Snacks.find()
        snack = [s for s in snacks if s['_id'] == ObjectId(type_id)]
        if len(snack) == 0:
            return jsonify({'error':'record not found'})
        else:
            for s in snack:
                old.append({'name':s['name'], 'id':str(s['_id'])})

            #delete the filtered file
            db.Snacks.delete_many({'_id':ObjectId(type_id)})
        return jsonify({'message':'you have successfully deleted the record'}, {'record-deleted':old})
    except:
        return jsonify({'error':'enter a valid id'})

#


if __name__ == '__main__':
    app.run(debug=True)
