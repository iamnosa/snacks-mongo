# README #

This README would normally document whatever steps are necessary to get your application up and running.

### A simple API built with Flask and MongoDB ###

## Communicating with the API ##

Setup your Python Environment and then install the necessary requirements from the "requirements.txt" file.


# Getting all Entries #

Send a GET Request to:

localhost:5000/api/v2/snacks/types

Where <type_id> is the id of the entry. i.e. ObjectID


# Getting a particular entry #

Send a GET Request to:

localhost:5000/api/v2/snacks/types/<type_id>

Where <type_id> is the id of the entry. i.e. ObjectID


# Setting a new entry #

Send a POST Request to:

localhost:5000/api/v2/snacks/types

with the JSON Structure as:
{
 'name':name,
 'rate':4.2,
 'description':description,
 'in-stock':true
}


# Updating an entry #

Send a PUT or PATCH Request to:

localhost:5000/api/v2/snacks/types/<type_id>

with the Update JSON Structure as:
{
 'name':name,
 'rate':2.0,
 'description':description,
 'in-stock':false
}



# Deleting an entry #

Send a DELETE Request to:

localhost:5000/api/v2/snacks/types/<type_id>